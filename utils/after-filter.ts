export const afterFilter = (after: string) => {
  return {
    "@timestamp":{
      "gte": after,
      "format":"strict_date_optional_time"
    }
  };
}
