import {DateRange} from "../date-range.type";

export const periodFilter = (period: DateRange) => {
  return {
    "@timestamp":{
      "gte": period.after,
      "lte": period.before,
      "format":"strict_date_optional_time"
    }
  };
}
