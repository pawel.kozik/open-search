import axios from "axios";

const getAccessToken = async (refreshToken: string): Promise<string> => {
  return (await axios.post(
    'http://private-alb.prod.kevin.internal/ob/v1/token?bankId=SEB_LT',
    {
      "grantType": "refresh_token",
      "refreshToken": refreshToken,
      "redirectUrl": "https://api.kevin.eu/platform/frame/callback/login/redirect",
      "state": "de945222-33e7-47a3-b530-0e0079080ac3"
    },
    {
      headers: {
        'Content-Type': "application/json",
      },
    }
  )).data.accessToken;
};

const getPaymentStatus = async (accessToken: string, productId: string, paymentId: string): Promise<string> => {
  return (await axios.get(
    `http://private-alb.prod.kevin.internal/ob/v1/payments/${productId}/${paymentId}/status?bankId=SEB_LT`,
    {
      headers: {
        'Content-Type': "application/json",
        'PSU-IP-Address': '52.29.172.54',
        'Authorization': `Bearer ${accessToken}`,
        'Trace-ID': 'eb52917d-86f1-4511-a89a-9ff4e276ffc7'
      },
    }
  )).data.status;
};

export const obCore = {
  getAccessToken,
  getPaymentStatus,
};
