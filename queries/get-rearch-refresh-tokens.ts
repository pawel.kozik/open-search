import {DateRange} from "../date-range.type";
import {fetchElastic} from "../opensearch/fetch-elastic";
import {cookie} from "../cookie";
import {periodFilter} from "../utils/period-filter";
import {afterFilter} from "../utils/after-filter";

export const getRearchRefreshTokens = async (psuId: string, period: DateRange) => {
  const data = {
    "params":{
      "index":"bank-integrations-0*",
      "body":{
        "version":true,
        "size":500,
        "sort":[
          {
            "@timestamp":{
              "order":"asc",
              "unmapped_type":"boolean"
            }
          }
        ],
        "aggs":{
          "2":{
            "date_histogram":{
              "field":"@timestamp",
              "fixed_interval":"12h",
              "time_zone":"UTC",
              "min_doc_count":1
            }
          }
        },
        "stored_fields":[
          "*"
        ],
        "script_fields":{

        },
        "docvalue_fields":[
          {
            "field":"@timestamp",
            "format":"date_time"
          },
          {
            "field":"meta.data_body_Data_CreationDateTime",
            "format":"date_time"
          },
          {
            "field":"meta.data_body_Data_StatusUpdateDateTime",
            "format":"date_time"
          },
          {
            "field":"meta.data_requestedExecutionDate",
            "format":"date_time"
          },
          {
            "field":"time",
            "format":"date_time"
          },
          {
            "field":"timestamp",
            "format":"date_time"
          }
        ],
        "_source":{
          "excludes":[

          ]
        },
        "query":{
          "bool":{
            "must":[

            ],
            "filter":[
              {
                "multi_match":{
                  "type":"phrase",
                  "query": psuId,
                  "lenient":true
                }
              },
              {
                "match_phrase":{
                  "involved":"POST /https://ob-core.prod.aws.kevin.eu/ob/v1/token"
                }
              },
              {
                "match_phrase":{
                  "event":"response"
                }
              },
              {
                "bool":{
                  "should":[
                    {
                      "match_phrase":{
                        "bankId.keyword":"SEB_LT"
                      }
                    },
                    {
                      "match_phrase":{
                        "bankId.keyword":"SEB_EE"
                      }
                    },
                    {
                      "match_phrase":{
                        "bankId.keyword":"SEB_LV"
                      }
                    }
                  ],
                  "minimum_should_match":1
                }
              },
              {
                "range":{
                  ...afterFilter(period.after),
                }
              }
            ],
            "should":[

            ],
            "must_not":[

            ]
          }
        },
        "highlight":{
          "pre_tags":[
            "@opensearch-dashboards-highlighted-field@"
          ],
          "post_tags":[
            "@/opensearch-dashboards-highlighted-field@"
          ],
          "fields":{
            "*":{

            }
          },
          "fragment_size":2147483647
        }
      },
      "preference":1688717059122
    }
  };

  const response = await fetchElastic(data, cookie);

  // @ts-ignore
  const refreshTokens = response.rawResponse.hits.hits.map(hit => hit['_source'].meta.data).map(data => JSON.parse(data)['refreshToken']);

  return refreshTokens as string[];
}
