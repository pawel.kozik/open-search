import {DateRange} from "../date-range.type";
import {fetchElastic} from "../opensearch/fetch-elastic";
import {cookie} from "../cookie";
import {periodFilter} from "../utils/period-filter";
import {afterFilter} from "../utils/after-filter";

export const getLegacyRefreshTokens = async (psuId: string, period: DateRange): Promise<{ refreshToken: string, traceId: string }[]> => {
  const data = {
    "params":{
      "index":"platform-frame-0*",
      "body":{
        "version":true,
        "size":500,
        "sort":[
          {
            "@timestamp":{
              "order":"desc",
              "unmapped_type":"boolean"
            }
          }
        ],
        "aggs":{
          "2":{
            "date_histogram":{
              "field":"@timestamp",
              "fixed_interval":"12h",
              "time_zone":"UTC",
              "min_doc_count":1
            }
          }
        },
        "stored_fields":[
          "*"
        ],
        "script_fields":{

        },
        "docvalue_fields":[
          {
            "field":"@timestamp",
            "format":"date_time"
          },
          {
            "field":"timestamp",
            "format":"date_time"
          }
        ],
        "_source":{
          "excludes":[

          ]
        },
        "query":{
          "bool":{
            "must":[

            ],
            "filter":[
              {
                "multi_match":{
                  "type":"phrase",
                  "query":psuId,
                  "lenient":true
                }
              },
              {
                "range":{
                  ...afterFilter(period.after),
                }
              }
            ],
            "should":[

            ],
            "must_not":[

            ]
          }
        },
        "highlight":{
          "pre_tags":[
            "@opensearch-dashboards-highlighted-field@"
          ],
          "post_tags":[
            "@/opensearch-dashboards-highlighted-field@"
          ],
          "fields":{
            "*":{

            }
          },
          "fragment_size":2147483647
        }
      },
      "preference":1688717725928
    }
  };

  const response = await fetchElastic(data, cookie);

  // @ts-ignore
  //const refreshTokens = response.rawResponse.hits.hits.map(hit => hit['_source'].message as string).map(msg => msg.replace('Response data: ', '')).map(data => JSON.parse(data)['body']['refreshToken']);
  // @ts-ignore
  const refreshTokens = response.rawResponse.hits.hits.map(hit => hit['_source'] as Record<string, unknown>).map(source => ({
    refreshToken: JSON.parse((source.message as string).replace('Response data: ', ''))['body']['refreshToken'],
    traceId: source.traceId,
  }));

  // @ts-ignore
  return refreshTokens;
}
