import {fetchElastic} from "../opensearch/fetch-elastic";
import {cookie} from "../cookie";
import {DateRange} from "../date-range.type";

function onlyUnique(value, index, array) {
  return array.indexOf(value) === index;
}

export const getFlowsWithTokenInvalid = async (period: DateRange): Promise<string[]> => {
  const data = {
    "params":{
      "index":"bank-integrations-0*",
      "body":{
        "version":true,
        "size":500,
        "sort":[
          {
            "@timestamp":{
              "order":"desc",
              "unmapped_type":"boolean"
            }
          }
        ],
        "aggs":{
          // "flowId": {
          //   "terms": {
          //     "field": "session"
          //   }
          // }
        },
        "stored_fields":[
          "*"
        ],
        "script_fields":{

        },
        "docvalue_fields":[
          {
            "field":"@timestamp",
            "format":"date_time"
          },
          {
            "field":"meta.data_body_Data_CreationDateTime",
            "format":"date_time"
          },
          {
            "field":"meta.data_body_Data_StatusUpdateDateTime",
            "format":"date_time"
          },
          {
            "field":"meta.data_requestedExecutionDate",
            "format":"date_time"
          },
          {
            "field":"time",
            "format":"date_time"
          },
          {
            "field":"timestamp",
            "format":"date_time"
          }
        ],
        "_source":{
          "excludes":[

          ]
        },
        "query":{
          "bool":{
            "must":[

            ],
            "filter":[
              {
                "multi_match":{
                  "type":"phrase",
                  "query":"TOKEN_INVALID",
                  "lenient":true
                }
              },
              {
                "match_phrase":{
                  "bankId.keyword":"SEB_LT"
                }
              },
              {
                "range":{
                  "@timestamp":{
                    "gte": period.after,
                    "lte": period.before,
                    "format":"strict_date_optional_time"
                  }
                }
              }
            ],
            "should":[

            ],
            "must_not":[

            ]
          }
        },
        "highlight":{
          "pre_tags":[
            "@opensearch-dashboards-highlighted-field@"
          ],
          "post_tags":[
            "@/opensearch-dashboards-highlighted-field@"
          ],
          "fields":{
            "*":{

            }
          },
          "fragment_size":2147483647
        }
      },
      "preference":1688709337007
    }
  };

  const response = await fetchElastic(data, cookie);

  // @ts-ignore
  return response.rawResponse.hits.hits.map(hit => hit['_source'].session).filter(onlyUnique);
}