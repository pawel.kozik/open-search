import {DateRange} from "../date-range.type";
import {fetchElastic} from "../opensearch/fetch-elastic";
import {cookie} from "../cookie";
import {periodFilter} from "../utils/period-filter";

export const getLegacyRefreshToken = async (traceId: string, period: DateRange): Promise<string> => {
  const data = {
    "params":{
      "index":"secure-0*",
      "body":{
        "version":true,
        "size":500,
        "sort":[
          {
            "@timestamp":{
              "order":"desc",
              "unmapped_type":"boolean"
            }
          }
        ],
        "aggs":{
          "2":{
            "date_histogram":{
              "field":"@timestamp",
              "fixed_interval":"12h",
              "time_zone":"UTC",
              "min_doc_count":1
            }
          }
        },
        "stored_fields":[
          "*"
        ],
        "script_fields":{

        },
        "docvalue_fields":[
          {
            "field":"@timestamp",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.Data.CreationDateTime",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.Data.StatusUpdateDateTime",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.balances.closingBooked.lastActionDateTime",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.paymentRequest.creationDateTime",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.paymentRequest.creditTransferTransaction.requestedExecutionDate",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.requestedExecutionDate",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.responseHeader.sendDate",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.scope_details.scopeTimeLimit",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.transactions.booked.bookingDate",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.transactions.booked.valueDate",
            "format":"date_time"
          },
          {
            "field":"message.meta.body.validTo",
            "format":"date_time"
          },
          {
            "field":"message.meta.headers.send-date",
            "format":"date_time"
          },
          {
            "field":"meta.headers.send-date",
            "format":"date_time"
          },
          {
            "field":"time",
            "format":"date_time"
          },
          {
            "field":"timestamp",
            "format":"date_time"
          }
        ],
        "_source":{
          "excludes":[

          ]
        },
        "query":{
          "bool":{
            "must":[

            ],
            "filter":[
              {
                "multi_match":{
                  "type":"phrase",
                  "query": `\t${traceId}`,
                  "lenient":true
                }
              },
              {
                "range":{
                  ...periodFilter(period),
                }
              }
            ],
            "should":[

            ],
            "must_not":[

            ]
          }
        },
        "highlight":{
          "pre_tags":[
            "@opensearch-dashboards-highlighted-field@"
          ],
          "post_tags":[
            "@/opensearch-dashboards-highlighted-field@"
          ],
          "fields":{
            "*":{

            }
          },
          "fragment_size":2147483647
        }
      },
      "preference":1688718234638
    }
  };

  const response = await fetchElastic(data, cookie);

  // @ts-ignore
  const res = response.rawResponse.hits.hits.map(hit => hit['_source']?.message?.meta?.body?.refresh_token || hit['_source']?.meta?.body?.refresh_token).filter(el => el !== undefined).pop();

  if (res === undefined) {
    return undefined;
    throw new Error('No refresh token found for traceId ' + traceId)
  };

  return res;
}
