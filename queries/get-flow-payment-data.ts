import {DateRange} from "../date-range.type";
import {fetchElastic} from "../opensearch/fetch-elastic";
import {cookie} from "../cookie";
import {periodFilter} from "../utils/period-filter";

export const getFlowPaymentData = async (flowId: string, period: DateRange) => {
  const data = {
    "params":{
      "index":"bank-integrations-0*",
      "body":{
        "version":true,
        "size":1,
        "sort":[
          {
            "@timestamp":{
              "order":"desc",
              "unmapped_type":"boolean"
            }
          }
        ],
        "aggs":{
          "2":{
            "date_histogram":{
              "field":"@timestamp",
              "fixed_interval":"12h",
              "time_zone":"UTC",
              "min_doc_count":1
            }
          }
        },
        "stored_fields":[
          "*"
        ],
        "script_fields":{

        },
        "docvalue_fields":[
          {
            "field":"@timestamp",
            "format":"date_time"
          },
          {
            "field":"meta.data_body_Data_CreationDateTime",
            "format":"date_time"
          },
          {
            "field":"meta.data_body_Data_StatusUpdateDateTime",
            "format":"date_time"
          },
          {
            "field":"meta.data_requestedExecutionDate",
            "format":"date_time"
          },
          {
            "field":"time",
            "format":"date_time"
          },
          {
            "field":"timestamp",
            "format":"date_time"
          }
        ],
        "_source":{
          "excludes":[

          ]
        },
        "query":{
          "bool":{
            "must":[

            ],
            "filter":[
              {
                "multi_match":{
                  "type":"phrase",
                  "query": flowId,
                  "lenient":true
                }
              },
              {
                "match_phrase":{
                  "involved":"POST /flows/execute HTTP/1.1"
                }
              },
              {
                "range":{
                  ...periodFilter(period),
                }
              }
            ],
            "should":[

            ],
            "must_not":[

            ]
          }
        },
        "highlight":{
          "pre_tags":[
            "@opensearch-dashboards-highlighted-field@"
          ],
          "post_tags":[
            "@/opensearch-dashboards-highlighted-field@"
          ],
          "fields":{
            "*":{

            }
          },
          "fragment_size":2147483647
        }
      },
      "preference":1688719736409
    }
  };

  const response = await fetchElastic(data, cookie);

  const hit = response.rawResponse.hits.hits.pop();

  if (!hit) {
    throw new Error('Unable to find payment data for flow ' + flowId);
  }

  // @ts-ignore
  const meta = hit['_source'].meta;
  const paymentId = meta.data_outcomes_paymentId as string;
  const productId = meta.data_state_extraDetails_paymentProduct as string;

  return {
    paymentId,
    productId,
  }
}
