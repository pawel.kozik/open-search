import {DateRange} from "../date-range.type";
import {fetchElastic} from "../opensearch/fetch-elastic";
import {cookie} from "../cookie";
import {periodFilter} from "../utils/period-filter";

export const getPsuId = async (flowId: string, period: DateRange) => {
  const data = {
    "params":{
      "index":"bank-integrations-0*",
      "body":{
        "version":true,
        "size":1,
        "sort":[
          {
            "@timestamp":{
              "order":"asc",
              "unmapped_type":"boolean"
            }
          }
        ],
        "aggs":{
          "2":{
            "date_histogram":{
              "field":"@timestamp",
              "fixed_interval":"12h",
              "time_zone":"UTC",
              "min_doc_count":1
            }
          }
        },
        "stored_fields":[
          "*"
        ],
        "script_fields":{

        },
        "docvalue_fields":[
          {
            "field":"@timestamp",
            "format":"date_time"
          },
          {
            "field":"meta.data_body_Data_CreationDateTime",
            "format":"date_time"
          },
          {
            "field":"meta.data_body_Data_StatusUpdateDateTime",
            "format":"date_time"
          },
          {
            "field":"meta.data_requestedExecutionDate",
            "format":"date_time"
          },
          {
            "field":"time",
            "format":"date_time"
          },
          {
            "field":"timestamp",
            "format":"date_time"
          }
        ],
        "_source":{
          "excludes":[

          ]
        },
        "query":{
          "bool":{
            "must":[

            ],
            "filter":[
              {
                "multi_match":{
                  "type":"phrase",
                  "query": flowId,
                  "lenient":true
                }
              },
              {
                "match_phrase":{
                  "involved":"POST /https://ob-core.prod.aws.kevin.eu/ob/v1/token"
                }
              },
              {
                "match_phrase":{
                  "event":"response"
                }
              },
              {
                "bool":{
                  "should":[
                    {
                      "match_phrase":{
                        "bankId.keyword":"SEB_LT"
                      }
                    },
                    {
                      "match_phrase":{
                        "bankId.keyword":"SEB_EE"
                      }
                    },
                    {
                      "match_phrase":{
                        "bankId.keyword":"SEB_LV"
                      }
                    }
                  ],
                  "minimum_should_match":1
                }
              },
              {
                "range":{
                  ...periodFilter(period),
                }
              }
            ],
            "should":[

            ],
            "must_not":[
              {
                "match_phrase":{
                  "meta.message":"Request failed with status code 400"
                }
              }
            ]
          }
        },
        "highlight":{
          "pre_tags":[
            "@opensearch-dashboards-highlighted-field@"
          ],
          "post_tags":[
            "@/opensearch-dashboards-highlighted-field@"
          ],
          "fields":{
            "*":{

            }
          },
          "fragment_size":2147483647
        }
      },
      "preference":1688713945263
    }
  };

  const response = await fetchElastic(data, cookie);

  // @ts-ignore
  const payload = response.rawResponse.hits.hits.pop()['_source'].meta.data;
  const psuId = JSON.parse(payload)['psuUserId'];

  return psuId as string;
}
