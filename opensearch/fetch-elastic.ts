import axios from "axios";

export type ElasticResponse = {
  isPartial: boolean;
  isRunning: boolean;
  rawResponse: {
    took: number;
    timed_out: number;
    _shards: any;
    hits: {
      total: number,
      max_score: any;
      hits: Record<string, unknown>[];
    },
    aggregations: object;
  },
  total: number;
  loaded: number;
}

export const fetchElastic = async (data: object, cookie: string): Promise<ElasticResponse> => {
  const response = await axios({
    method: 'POST',
    url: 'https://vpc-prod-opensearch-risj5mdrgwblk33eolyxxkpexi.eu-central-1.es.amazonaws.com/_dashboards/internal/search/opensearch',
    data,
    headers: {
      "accept": "*/*",
      "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
      "content-type": "application/json",
      "osd-version": "2.5.0",
      "sec-ch-ua": "\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\", \"Google Chrome\";v=\"114\"",
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": "\"macOS\"",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-origin",
      "cookie": cookie,
      "Referer": "https://vpc-prod-opensearch-risj5mdrgwblk33eolyxxkpexi.eu-central-1.es.amazonaws.com/_dashboards/app/discover",
      "Referrer-Policy": "strict-origin-when-cross-origin"
    }
  });

  return response.data;
};
