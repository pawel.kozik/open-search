import {getPsuId} from "../queries/get-psu-id";
import {getRearchRefreshTokens} from "../queries/get-rearch-refresh-tokens";
import {getLegacyRefreshTokens} from "../queries/get-legacy-refresh-tokens";
import {getLegacyRefreshToken} from "../queries/get-legacy-refresh-token";
import {getFlowPaymentData} from "../queries/get-flow-payment-data";
import axios from "axios";
import {obCore} from "../utils/ob-core";
import * as fs from "fs";
import {getFlowsWithTokenInvalid} from "../queries/get-flows-with-token-invalid";
import * as os from "os";

const resolveFlowData = async (flowId: string, period) => {
  console.log('Resolve data for flow ' + flowId);

  const psuId = await getPsuId(flowId, period);
  const rearchRefreshTokens = await getRearchRefreshTokens(psuId, period);
  const legacyTokensIds = (await getLegacyRefreshTokens(psuId, period)).map(({ traceId }) => traceId);
  const legacyRefreshTokens = await Promise.all(legacyTokensIds.map( (traceId) => getLegacyRefreshToken(traceId, period)));
  const paymentData = await getFlowPaymentData(flowId, period);

  return {
    flowId,
    psuId,
    rearchRefreshTokens,
    legacyRefreshTokens,
    paymentData,
  }
};

const resolveRefreshToken = (rearchRefreshTokens: string[], legacyRefreshTokens: string[]): string => {
  if (legacyRefreshTokens.length > 0) {
    const res = legacyRefreshTokens[0];

    if (res === undefined) {
      throw new Error('Refresh token is missing. Most likely because SecureService didnt had this log');
    }

    return res;
  }

  return rearchRefreshTokens[rearchRefreshTokens.length - 1];
}

/**
 * Find occurrences of invalid token.
 * Returns flowIds.
 */
export const tokenInvalid = async () => {
  const period = {
    after: '2023-06-27T00:00:00.000Z',
    before: '2023-07-03T23:30:00.000Z',
  }

  const flowIds = await getFlowsWithTokenInvalid(period);

  for (const flowId of flowIds) {
    const data = await resolveFlowData(flowId, period);

    let line = `FlowId: ${flowId} `;

    try {
      const refreshToken = resolveRefreshToken(data.rearchRefreshTokens, data.legacyRefreshTokens);
      const accessToken = await obCore.getAccessToken(refreshToken);

      const status = await obCore.getPaymentStatus(
        accessToken,
        data.paymentData.productId,
        data.paymentData.paymentId,
      );
      line += `PaymentId: ${data.paymentData.paymentId} Status: ${status}`
    } catch (err) {
      if (err.message === 'Refresh token is missing. Most likely because SecureService didnt had this log') {
        line += `PaymentId: ${data.paymentData.paymentId} Status: Failed to find working refresh token`;
      } else {
        line += `PaymentId: ${data.paymentData.paymentId} Status: Unknown error`;
      }
    }

    line += os.EOL;
    fs.appendFileSync('token-invalid-result.json', line);
  }

  // const flowId = 'flw_0OXjHWcf_YelmWBsv9W9zT';
  // const data = await resolveFlowData(flowId, period);
  // const refreshToken = resolveRefreshToken(data.rearchRefreshTokens, data.legacyRefreshTokens);
  // const accessToken = await obCore.getAccessToken(refreshToken);
  //
  // const status = await obCore.getPaymentStatus(
  //   accessToken,
  //   data.paymentData.productId,
  //   data.paymentData.paymentId,
  // );
  //
  // console.log(status)
}
