export type DateRange = {
  after: string;
  before: string;
};
